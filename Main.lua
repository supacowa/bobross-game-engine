function setup()
    vp_type_btn = ViewportTypeButton(WIDTH - 64, HEIGHT - 28, 24)
    vp_type_btn.inactiveColor = color(127)
    vp_type_btn.activeColor = color(50)
    vp_close_btn = ViewportCloseButton(WIDTH - 28, HEIGHT - 28, 24)
end



function draw()
    background(127)
    noFill()
    strokeWidth(3)
    stroke(100)
    line(0, HEIGHT - 32, WIDTH, HEIGHT - 32)
    strokeWidth(0)
    fill(100)
    ellipseMode(CORNER)
    ellipse(WIDTH - 68, HEIGHT - 32, 32)
    ellipse(WIDTH - 32, HEIGHT - 32, 32)
    vp_type_btn:draw()
    vp_close_btn:draw()
end




function touched(touch)
    if touch.state == ENDED then
        vp_type_btn:testCollision(touch, vp_type_btn.toggle, vp_type_btn)
        vp_close_btn:testCollision(touch, vp_close_btn.toggle, vp_close_btn)
    end
end
