EmptyViewport = class()

function EmptyViewport:init()
    self.width = 1
    self.height = 1
    self.bgColor = color(46, 81, 200, 255)
    self.fgColor = color(138, 158, 250, 255)
    self.borderWidth = 2
    self.viewType = self
end

function EmptyViewport:draw()
    pushStyle()
    strokeWidth(self.borderWidth)
    stroke(self.fgColor)
    fill(self.bgColor)
    rect(0, 0, WIDTH * self.width, HEIGHT * self.height)
    fill(self.fgColor)
    rect(0, HEIGHT * self.height - 32, WIDTH * self.width, 32)
    popStyle()
end

function EmptyViewport:touched(touch)
end








Desktop = class()


function Desktop:init()
    self.viewports = {}
end


function Desktop:draw()
    for id, view in ipairs(self.viewports) do
        
    end
end
