ColorTable = class()


function ColorTable:init(rgbaColorList)
    self.list = rgbaColorList or {
        color(0, 0, 0, 255),
        color(255, 255, 255, 255)
    }
end


function ColorTable:add(rgbaColor)
    table.insert(self.list, rgbaColor)
end


function ColorTable:remove(colorIndex)
    table.remove(self.list, colorIndex)
end


function ColorTable:clear()
    self.list = {}
end


function ColorTable:background(index)

    if not index then return self.colorTable[self.bgIndex] end
    self.bgIndex = index
end


function ColorTable:foreground(index)

    if not index then return self.colorTable[self.fgIndex] end
    self.fgIndex = index
end
