CGTrigger = class(CGStroke)

function CGTrigger:init(x, y, width, height)
    CGStroke.init(self, x, y, width, height)
    self.fillColor.a = 0 -- transparent
    self.animateRotation = nil -- rotation not supported
    self.resetRotationAnimation = nil
end

function CGTrigger:debugDraw()
    CGStroke.debugDraw(self)
    if self.debug then
        pushStyle()
        pushMatrix()
        
        translate(self.position.x, self.position.y)
        rotate(self.rotation)
        translate(-self.size.x * self.pivot.x, -self.size.y * self.pivot.y)
        
        noFill()
        stroke(0)
        strokeWidth(2)
        rectMode(CORNER)
        rect(0, 0, self.size.x, self.size.y)
        
        popMatrix()
        popStyle()
    end
end

function CGTrigger:draw()
    if self.rotation ~= 0 then self.rotation = 0 end -- triggers are always AABB
    if self.cornerDiameter ~= 0 then self.cornerDiameter = 0 end
    CGStroke.draw(self)
end

function CGTrigger:testCollision(object, action, ...) -- simple AABB hit test
    local cx1 = self.pivot.x
    local cy1 = self.pivot.y
    local w1 = self.size.x
    local h1 = self.size.y
    local x1 = self.position.x
    local y1 = self.position.y
    local cx2, cy2, w2, h2, x2, y2
    
    if object.pivot then cx2, cy2 = object.pivot.x or 0, object.pivot.y or 0 end -- property mapping for different types of objects, e.g. CGStroke vs. touch
    if not cx2 or not cy2 then cx2, cy2 = 0, 0 end
    if object.size then w2, h2 = object.size.x or 0, object.size.y or 0 end
    if not w2 or not h2 then w2, h2 = object.width or 0, object.height or 0 end
    if object.position then x2, y2 = object.position.x, object.position.y end
    if not x2 or not y2 then x2, y2 = object.x, object.y end
    
    assert(x1 and y1 and w1 and h1 and cx1 and cy1 and x2 and y2 and w2 and h2 and cx2 and cy2, "can not test collision because of missing parameters")
    
    local leftA = x1 - w1 * cx1 -- first rectangle edges
    local rightA = leftA + w1
    local lowerA = y1 - h1 * cy1
    local upperA = lowerA + h1
    local leftB = x2 - w2 * cx2 -- second rectangle edges
    local rightB = leftB + w2
    local lowerB = y2 - h2 * cy2
    local upperB = lowerB + h2
    local testResult = leftA <= rightB and rightA >= leftB and lowerA <= upperB and upperA >= lowerB
    
    if testResult and type(action) == "function" then action(self, ...) end
    return testResult
end








CGStroke = class()

function CGStroke:init(x, y, width, height)
    self.position = vec2(x, y)
    self.size = vec2(width, height)
    self.pivot = vec2()
    self.rotation = 0
    self.cornerDiameter = 0
    self.fillColor = color(255)
    self.animationEasingType = tween.easing.expoIn
    self.isVisible = true
    self.debug = false
end

function CGStroke:isAnimating()
    return
        (type(self.positionAnimator) ~= "nil" and self.positionAnimator.running < self.positionAnimator.time)
        or (type(self.sizeAnimator) ~= "nil" and self.sizeAnimator.running < self.sizeAnimator.time)
        or (type(self.pivotAnimator) ~= "nil" and self.pivotAnimator.running < self.pivotAnimator.time)
        or (type(self.rotationAnimator) ~= "nil" and self.rotationAnimator.running < self.rotationAnimator.time)
        or (type(self.colorAnimator) ~= "nil" and self.colorAnimator.running < self.colorAnimator.time)
end

function CGStroke:rotateTowards(x, y)
    -- calculate angle from this position towards a point
    local pointer = vec2(x, y) - self.position
    local angle = math.deg(math.atan2(pointer.y, pointer.x))
    if angle < 0 then angle = angle + 360 end
    self.rotation = (angle + 360) % 360
    return self.rotation
end

function CGStroke:anchorAtPivot(x, y)
    -- change pivot while retain the world position and rotation
    -- that is, the object stays visually in place even though its pivot changes
    local rad = math.rad(self.rotation)
    local sin = math.sin(rad)
    local cos = math.cos(rad)
    local pivotDelta = vec2(x, y) - self.pivot
    local pivotPosition = self.position + vec2(self.size.x * pivotDelta.x, self.size.y * pivotDelta.y)
    self.pivot = vec2(x, y)
    self.position = vec2(
        self.position.x + (cos * (pivotPosition.x - self.position.x) - sin * (pivotPosition.y - self.position.y)),
        self.position.y + (sin * (pivotPosition.x - self.position.x) + cos * (pivotPosition.y - self.position.y))
    )
    return self.position
end

function CGStroke:debugDraw()
    if self.debug then
        pushStyle()
        pushMatrix()
        translate(self.position.x, self.position.y)
        
        noFill()
        stroke(0)
        strokeWidth(2)
        lineCapMode(ROUND)
        
        rotate(45)
        line(-5, 0, 5, 0)
        rotate(-90)
        line(-5, 0, 5, 0)
        
        popMatrix()
        popStyle()
    end
end

function CGStroke:draw()
    if self.isVisible then
        pushStyle()
        pushMatrix()
        
        translate(self.position.x, self.position.y)
        rotate(self.rotation)
        translate(-self.size.x * self.pivot.x, -self.size.y * self.pivot.y)
        
        noStroke()
        fill(self.fillColor)
        ellipseMode(CORNER)
        rectMode(CORNER)
        
        if self.cornerDiameter <= 0 then
            rect(0, 0, self.size.x, self.size.y)
        else
            rect(self.cornerDiameter / 2, 0, self.size.x - self.cornerDiameter, self.size.y)
            rect(0, self.cornerDiameter / 2, self.size.x, self.size.y - self.cornerDiameter)
            ellipse(0, 0, self.cornerDiameter)
            ellipse(self.size.x - self.cornerDiameter, 0, self.cornerDiameter)
            ellipse(self.size.x - self.cornerDiameter, self.size.y - self.cornerDiameter, self.cornerDiameter)
            ellipse(0, self.size.y - self.cornerDiameter, self.cornerDiameter)
        end
        
        popMatrix()
        popStyle()
        
        self:debugDraw()
        
        --if self.rotationAnimator then print(self.rotationAnimator.running) end
    end
end

function CGStroke:animatePosition(x, y, duration, callback)
    self.positionAnimator = tween(duration, self.position, {x = x, y = y}, self.animationEasingType, function()
        if callback then callback() end
    end)
end

function CGStroke:animateSize(width, height, duration, callback)
    self.sizeAnimator = tween(duration, self.size, {x = width, y = height}, self.animationEasingType, function()
        if callback then callback() end
    end)
end

function CGStroke:animatePivot(x, y, duration, callback)
    self.pivotAnimator = tween(duration, self.pivot, {x = x, y = y}, self.animationEasingType, function()
        if callback then callback() end
    end)
end

function CGStroke:animateRotation(angle, duration, callback)
    self.rotationAnimator = tween(duration, self, {rotation = angle}, self.animationEasingType, function()
        if callback then callback() end
    end)
end

function CGStroke:animateColor(colour, duration, callback)
    self.colorAnimator = tween(duration, self.fillColor, {r = colour.r, g = colour.g, b = colour.b, a = colour.a}, self.animationEasingType, function()
        if callback then callback() end
    end)
end

function CGStroke:resetPositionAnimation()
    tween.reset(self.positionAnimator)
end

function CGStroke:resetSizeAnimation()
    tween.reset(self.sizeAnimator)
end

function CGStroke:resetPivotAnimation()
    tween.reset(self.pivotAnimator)
end

function CGStroke:resetRotationAnimation()
    tween.reset(self.rotationAnimator)
end

function CGStroke:resetColorAnimation()
    tween.reset(self.colorAnimator)
end








ViewportTypeButton = class(CGTrigger)

function ViewportTypeButton:init(x, y, size)
    CGTrigger.init(self, x, y, size, size)
    --self.fillColor = self.inactiveColor
    self.inactiveColor = color(50)
    self.activeColor = color(215)
    self.isActive = false
    self.isAnimating = function(this)
        return
            this.strokeA:isAnimating()
            or this.strokeB:isAnimating()
            or CGTrigger.isAnimating(this)
    end
    self.strokeA = CGStroke(0, 0, 0, 4)
    self.strokeA.cornerDiameter = 4
    self.strokeB = CGStroke(0, 0, 0, 4)
    self.strokeB.cornerDiameter = 4
end

function ViewportTypeButton:draw()
    CGTrigger.draw(self)
    if self.isVisible then
        if not self:isAnimating() then
            local buttonHalfWidth = self.size.x / 2
            if not self.isActive then
                local slopeLength = math.sqrt((buttonHalfWidth) ^ 2 + self.size.y ^ 2)
                self.strokeA.position = self.position + vec2(buttonHalfWidth, 0)
                self.strokeA.size.x = slopeLength
                self.strokeA.pivot = vec2(0, .5)
                self.strokeA:rotateTowards((self.position + vec2(0, self.size.y)):unpack())
                self.strokeA.fillColor = self.inactiveColor
                self.strokeB.position = self.position + vec2(buttonHalfWidth, 0)
                self.strokeB.size.x = slopeLength
                self.strokeB.pivot = vec2(0, .5)
                self.strokeB:rotateTowards((self.position + self.size):unpack())
                self.strokeB.fillColor = self.inactiveColor
            else
                self.strokeA.pivot = vec2(1, .5)
                self.strokeA.rotation = 0
                self.strokeA.position = self.position + self.size
                self.strokeA.size.x = self.size.x
                self.strokeA.fillColor = self.activeColor
                self.strokeB.pivot = vec2(0, .5)
                self.strokeB.rotation = 90
                self.strokeB.position = self.position + vec2(buttonHalfWidth, 0)
                self.strokeB.size.x = self.size.y
                self.strokeB.fillColor = self.activeColor
            end
        end
        self.strokeB:draw()
        self.strokeA:draw()
    end
end

function ViewportTypeButton:activate(action)
    self.strokeA:anchorAtPivot(1, .5)
    self.strokeA:animateSize(self.size.x, self.strokeA.size.y, .2)
    self.strokeA:animateRotation(180, .3, function()
        self.strokeB:animateSize(self.size.y, self.strokeB.size.y, .4)
        self.strokeB:animateRotation(90, .4, function()
            self.isActive = true
            if type(action) == "function" then action(self) end
        end)
    end)
    self.strokeA:animateColor(self.activeColor, .3, function()
        self.strokeB:animateColor(self.activeColor, .25)
    end)
    --self:animateColor(self.inactiveColor, .8)
end

function ViewportTypeButton:deactivate(action)
    local slopeLength = math.sqrt((self.size.x / 2) ^ 2 + self.size.y ^ 2)
    local currentAngle = self.strokeA.rotation
    local targetAngle = self.strokeA:rotateTowards(self.position.x + self.size.x / 2, self.position.y)
    self.strokeA.rotation = currentAngle
    self.strokeA:animateSize(slopeLength, self.strokeA.size.y, .4)
    self.strokeA:animateRotation(targetAngle - 180, .4, function()
        currentAngle = self.strokeB.rotation
        targetAngle = self.strokeB:rotateTowards(self.position.x, self.position.y + self.size.y)
        self.strokeB.rotation = currentAngle
        self.strokeB:animateSize(slopeLength, self.strokeB.size.y, .2)
        self.strokeB:animateRotation(targetAngle, .2, function()
            self.isActive = false
            if type(action) == "function" then action(self) end
        end)
    end)
    --self.strokeA:animateColor(self.inactiveColor, .3, function()
        --self.strokeB:animateColor(self.inactiveColor, .3)
    --end)
    --self:animateColor(self.activeColor, .8)
end

function ViewportTypeButton:toggle(action)
    if not self:isAnimating() then
        if self.isActive then
            self:deactivate(action)
        else
            self:activate(action)
        end
    end
end








ViewportCloseButton = class(CGTrigger)

function ViewportCloseButton:init(x, y, size)
    CGTrigger.init(self, x, y, size, size)
end
